<?php

$page_id = "used-page";

$typegroup = varius_get_queried_typegroup();
$type = varius_get_queried_type();

get_header();
?>

<div id="<?php echo $page_id ?>" class="body-wrap">

        <div id="body-container">
            <div id="content">

    <h1>Used Equipment from Maple Lane - Category: <?php echo $type ? $type->name : $typegroup->name ?></h1>
    <div id="used-equipment">
        <div class="breadcrumb">
            <?php if ( $typegroup ) : ?>
                <select onchange="if (this.value) window.location.href=this.value">
                    <option <?php echo $type ? '' : 'selected' ?> value="<?php echo varius_wp_url(['typegroup_code' => $typegroup->code]) ?>"><?php echo "{$typegroup->name} ({$typegroup->count})"?></option>
                    <?php foreach ( $typegroup->types as $T ) : ?>
                        <option <?php echo $type && $T->code == $type->code ? 'selected' : '' ?> value="<?php echo $T->url ?>"><?php echo "-- {$T->name} ({$T->count})"?></option>
                    <?php endforeach ?>
                </select>
                <select onchange="if (this.value) window.location.search = '?varius-wp-order='+this.value">
                    <?php $o = varius_get_sort_order() ?>
                    <option value="ud" <?php echo $o == 'ud' ? 'selected' : '' ?>>Newest First</option>
                    <option value="ua" <?php echo $o == 'ua' ? 'selected' : '' ?>>Oldest First</option>
                    <option value="pa" <?php echo $o == 'pa' ? 'selected' : '' ?>>Price Ascending</option>
                    <option value="pd" <?php echo $o == 'pd' ? 'selected' : '' ?>>Price Descending</option>
                    <option value="sna" <?php echo $o == 'sna' ? 'selected' : '' ?>>Stock Number</option>
                </select>
            <?php endif ?>
        </div>
        <div id="listings">
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); $listing = varius_get_post_listing() ?>
                    <div class="listing">
                        <a href="<?php echo esc_url($listing->url) ?>"><?php echo $listing->title ?></a>
                    </div>
                <?php endwhile ?>
            <?php else : ?>
                <p>There are currently no listings in the '<?php echo $typegroup->name . ( $type ? ' - ' . $type->name : '' ) ?>' category</p>
            <?php endif ?>
        </div>
        <?php echo paginate_links(['total' => $wp_query->max_num_pages, 'current' => max(1, get_query_var('paged'))]) ?>
    </div>

        </div><!-- #content -->
    </div><!-- #body-container -->
</div><!-- #page_id.body-wrap -->
<?php get_footer(); ?>

