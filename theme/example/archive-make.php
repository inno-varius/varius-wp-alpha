<?php

$page_id = "used-page";

$order = isset($_GET['order']) ? $_GET['order'] : 'ud';

$make = varius_get_queried_make();

get_header();
?>

<div id="<?php echo $page_id ?>" class="body-wrap">

        <div id="body-container">
            <div id="content">

    <h1>Used <?php echo $make->name ?> Equipment from Maple Lane</h1>
    <select onchange="if (this.value) window.location.search = '?varius-wp-order='+this.value">
        <?php $o = varius_get_sort_order() ?>
        <option value="ud" <?php echo $o == 'ud' ? 'selected' : '' ?>>Newest First</option>
        <option value="ua" <?php echo $o == 'ua' ? 'selected' : '' ?>>Oldest First</option>
        <option value="pa" <?php echo $o == 'pa' ? 'selected' : '' ?>>Price Ascending</option>
        <option value="pd" <?php echo $o == 'pd' ? 'selected' : '' ?>>Price Descending</option>
        <option value="sna" <?php echo $o == 'sna' ? 'selected' : '' ?>>Stock Number</option>
    </select>
    <div id="used-equipment">
        <div id="listings">
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); $listing = varius_get_post_listing() ?>
                    <div class="listing">
                        <a href="<?php echo esc_url($listing->url) ?>"><?php echo $listing->title ?> (<?php echo $listing->stock_number ?>)</a> <?php echo "$" . number_format($listing->price->amount,0) . " " . $listing->meta->updated ?>
                    </div>
                <?php endwhile ?>
            <?php else : ?>
                <p>There are currently no listings of make '<?php echo $make->name ?>'</p>
            <?php endif ?>
        </div>
        <?php echo paginate_links(['total' => $wp_query->max_num_pages, 'current' => max(1, get_query_var('paged'))]) ?>
    </div>

        </div><!-- #content -->
    </div><!-- #body-container -->
</div><!-- #page_id.body-wrap -->
<?php get_footer(); ?>

