<?php

$page_id = "used-page";

get_header();

the_post();

$listing = varius_get_post_listing();

// Add some links to the title if possible
$title = $listing->title;

if ( $listing->type->api_typegroup ) {
    $url = varius_wp_url(['typegroup_code' => $listing->type->api_typegroup->code, 'type_code' => $listing->type->code]);
    $title = preg_replace("/". $listing->type->name ."/", "<a href=\"$url\">$0</a>", $listing->title);
}
if ( $listing->attributes->make) {
    $url = varius_wp_url(['make_code' => $listing->attributes->make->code]);
    $title = preg_replace("/". $listing->attributes->make->name ."/", "<a href=\"$url\">$0</a>", $title);
}

?>

<div id="<?php echo $page_id ?>" class="body-wrap">

    <div id="body-container">
        <div id="content">

        <h1><?php echo $title ?></h1>
        <div class="listing">
            <img src="<?php echo $listing->images[0]->full_uri ?>" alt="">
            <?php echo "$" . number_format($listing->price->amount) ?>
            <div class="desc">
                <?php echo $listing->description ?>
            </div>
        </div>

        </div><!-- #content -->
    </div><!-- #body-container -->
</div><!-- #page_id.body-wrap -->
<?php get_footer(); ?>

