<?php

$page_id = "used-page";

get_header();

the_post();

?>

<div id="<?php echo $page_id ?>" class="body-wrap">

    <div id="body-container">
        <div id="content">

        <h2><?php the_title() ?></h2>
        <div class="error-message">
            <?php the_content() ?>
        </div>

        </div><!-- #content -->
    </div><!-- #body-container -->
</div><!-- #page_id.body-wrap -->

<?php get_footer(); ?>

