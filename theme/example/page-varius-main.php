<?php

$page_id = "used-page";

$makes = varius_api_get_makes_ranked();
$_makes = [];
foreach ( $makes as $m ) {
    $_makes[$m->code] = $m;
}
$makes = $_makes;

$highlighted_makes = [ 'fendt', 'hardi', 'massey_ferguson', 'other' ];

get_header();
?>

<div id="<?php echo $page_id ?>" class="body-wrap">

        <div id="body-container">
            <div id="content">

    <h1>Used Equipment from Maple Lane</h1>
    <?php if ( isset($_GET['unknown_listing']) ) : ?>
        <div class="alert">The listing you were looking for is no longer available.  Browse our other listings below!</div>
    <?php endif ?>
    <div id="used-equipment">
        <div id="categories">
            <?php foreach ( varius_api_get_typegroups_ranked() as $typegroup ) : ?>
                <div class="category"><a href="<?php echo esc_url($typegroup->url) ?>"><?php echo $typegroup->name ?> (<?php echo $typegroup->count ?>)</a>
                    <select name="subcategories_<?php echo $typegroup->slug ?>" onchange="if (this.value) window.location.href=this.value" class="subcategories">
                        <option value="">-- or pick a subtype --</option>
                        <?php foreach ( $typegroup->types as $type ) : ?>
                            <option value="<?php echo $type->url ?>"><?php echo $type->name ?> (<?php echo $type->count ?>)</option>
                        <?php endforeach ?>
                    </select>
                </div>
            <?php endforeach ?>
        </div>
        <div class="makes">
            <?php foreach ( $highlighted_makes as $make_code ) : ?>
                <div class="make">
                    <?php if ( $make_code != 'other' ) : ?>
                        <a href="<?php echo esc_url($makes[$make_code]->url) ?>"><?php echo $makes[$make_code]->name . "({$makes[$make_code]->count})" ?></a>
                    <?php else: $count = 0 ?>
                        <?php foreach ( $makes as $m ) : ?>
                            <?php if ( ! in_array($m->code, $highlighted_makes) ) {
                                $count += $m->count;
                            } ?>
                        <?php endforeach ?>
                        <select onchange="if (this.value) window.location.href=this.value">
                            <option value="">--- Other (<?php echo $count ?>)---</option>
                            <?php foreach ( $makes as $m ) : ?>
                                <?php if ( ! in_array($m->code, $highlighted_makes) ) : ?>
                                    <option value="<?php echo $m->url ?>"><?php echo "{$m->name} ({$m->count})" ?></option>
                                <?php endif ?>
                            <?php endforeach ?>
                        </select>
                    <?php endif ?>
                </div>
            <?php endforeach ?>
        </div>
    </div>

        </div><!-- #content -->
    </div><!-- #body-container -->
</div><!-- #page_id.body-wrap -->

<?php get_footer(); ?>

