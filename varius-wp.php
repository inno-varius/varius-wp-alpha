<?php
/**
 * Plugin Name:       Varius WP
 * Description:       Integrates your WordPress site with the Varius API
 * Version:           0.1.0
 * Author:            Varius
 * Author URI:        http://www.varius.ca
 * License:           MIT
 * License URI:       https://opensource.org/licenses/MIT
 * Text Domain:       varius-wp
 * Domain Path:       /lang
 */

require __DIR__ . '/vendor/autoload.php'; // Use composer's autoload
//require __DIR__ . '/vendor/variusapi/src/VariusApi.php';
use VariusApi\VariusApi;
use Helpers\VirtualPager;

if ( ! defined( 'ABSPATH' ) ) { die; } // If this file is called directly, abort.

define('VARIUS_WP_OPTION_NAME', 'varius_wp_options');
define('VARIUS_WP_OPTION_VERSION', 'varius_wp_version');
define('VARIUS_WP_VERSION', '0.1.0');


if ( ! class_exists('VariusWP') ) :

class VariusWP {

    protected $httpClient;

    protected $config;
    protected $option_data;

    protected function __construct() {}

    protected static $instance = null;
    public static function instance() {
        if ( self::$instance === null ) {
            self::$instance = new VariusWP();
            self::$instance->init();
        }
        return self::$instance;
    }

    public function init() {

        if ( ! session_id() ) { session_start(); }  // so we can maintain sort order on lists of listing

        if ( get_option(VARIUS_WP_OPTION_VERSION) != VARIUS_WP_VERSION ) {
            // make sure database values get upgraded for new versions if necessary
        }
        update_option(VARIUS_WP_OPTION_VERSION, VARIUS_WP_VERSION);

        $this->config = array(
                'capability' => 'manage_options'
            );

        $defaults = array(
                'url_base'          => '',
                'api_base_uri'      => '',
                'api_access_token'  => '',
                'dealer_code'       => ''
            );

        if ( is_array(get_option(VARIUS_WP_OPTION_NAME)) ) {
            $this->option_data = array_merge($defaults, get_option(VARIUS_WP_OPTION_NAME));
        } else {
            $this->option_data = $defaults;
        }

        register_activation_hook(__FILE__, array($this, 'activate'));
        register_deactivation_hook(__FILE__, array($this, 'deactivate'));
        register_uninstall_hook('VariusWP', 'uninstall');

        // $defaults = array(

        //     // basic
        //     'name'              => __('Varius WP', 'varius-wp'),
        //     'version'           => '0.1.0',

        //     // urls
        //     'basename'          => plugin_basename( __FILE__ ),
        //     'path'              => plugin_dir_path( __FILE__ ),
        //     'dir'               => plugin_dir_url( __FILE__ ),

        //     // options
        //     'capability'        => 'manage_options',
        //     'varius_api_key'    => '',

        //     'api_base_uri'      => 'http://varius.dev/api/',
        // );

//die(var_dump($this->option_data));
//die(var_dump($this->getSetting('api_base_uri')));
        $this->api = new VariusApi($this->getSetting('dealer_code'), $this->getSetting('api_base_uri'), $this->getSetting('api_access_token'));

        if ( is_admin() ) {
            include_once('admin/init.php');
        }

        include_once('inc/wp-api.php');

        new VirtualPager($this->getSetting('base_url'));

    }

    public function getSetting($name, $value = null) {
        // check settings
        if( isset($this->option_data[$name]) ) {
            $value = $this->option_data[$name];
        }

        return $value;
    }

    public function config($name, $value = null) {
        // check settings
        if( isset($this->config[$name]) ) {
            $value = $this->config[$name];
        }

        return $value;
    }

    public function setSetting($name, $value) {
        $this->option_data[ $name ] = $value;
    }

    public function activate() {
        update_option(VARIUS_WP_OPTION_NAME, $this->option_data);
        flush_rewrite_rules();
    }

    public static function deactivate() {
        delete_option(VARIUS_WP_OPTION_NAME);
        flush_rewrite_rules();
    }

    public static function uninstall() {
        delete_option(VARIUS_WP_OPTION_VERSION);
    }

}

endif;

function varius_wp() { return VariusWP::instance(); }
varius_wp();
