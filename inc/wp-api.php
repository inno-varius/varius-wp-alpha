<?php

function varius_get_post_listing() {
    global $post;

    if ( isset($post->varius_listing) ) { return $post->varius_listing; }

    return null;
}

function varius_get_queried_make() {
    global $post;

    $obj = get_queried_object();
    if ( isset($obj->varius_make) ) { return $obj->varius_make; }

    return null;
}

function varius_get_queried_typegroup() {

    $obj = get_queried_object();

    if ( isset($obj->varius_typegroup) ) { return $obj->varius_typegroup; }

    return null;
}

function varius_get_queried_type() {

    $obj = get_queried_object();

    if ( isset($obj->varius_type) ) { return $obj->varius_type; }

    return null;
}

function varius_get_sort_order() {

    if ( get_query_var('varius-wp-order') ) {
        $order = get_query_var('varius-wp-order');
        // if there's an order specified in the query string, set the session variable so we can use it later if needs be
        $_SESSION['varius-wp-order'] = $order;
        return $order;
    } elseif ( isset($_SESSION['varius-wp-order']) ) {
        // if no order is set in the query string, try using the order from the session
        return $_SESSION['varius-wp-order'];
    } else {
        // default to 'update date desc'
        return 'pd';
    }

}

function varius_wp_url($data = []) {
    $url = home_url() . trailingslashit(varius_wp()->getSetting('url_base'));

    if ( isset($data['type_code']) && isset($data['typegroup_code']) ) {
        $url .= 'type/' . $data['typegroup_code'] . '/' . $data['type_code'];
    } elseif ( isset($data['typegroup_code']) ) {
        $url .= 'type/' . $data['typegroup_code'];
    } elseif ( isset($data['stock_number']) ) {
        $url .= 'listing/' . urlencode($data['stock_number']);
    } elseif ( isset($data['make_code']) ) {
        $url .= 'make/' . $data['make_code'];  // don't urlencode or else userdefined makes don't work right
    }

    return $url;
}

function varius_api_status() {
    return varius_wp()->api->status();
}

function varius_api_test() {

    // make sure we can connect to the API
    $r = varius_wp()->api->status();

    // also make sure we can authenticate
    return varius_wp()->api->authTest();
}

function varius_api_get_makes() {
    $ret = varius_wp()->api->getMakes();

    return $ret->body->data;
}

function varius_api_get_make_ranked($make_code, $min_count=1) {
    $ret = varius_wp()->api->getMakeRanked($make_code, $min_count);

    $make = $ret->body->data[0];
    if ( $make ) {
        $make->url = varius_wp_url(['make_code' => $make->code]);
    }
    return $make;
}

function varius_api_get_makes_ranked($min_count=1) {
    $ret = varius_wp()->api->getMakesRanked($min_count);

    $makes = $ret->body->data;

    foreach ($makes as $m) {
        $m->url = varius_wp_url(['make_code' => $m->code]);
    }

    return $makes;
}

function varius_api_get_types() {
    $ret = varius_wp()->api->getTypes();

    return $ret->body->data;
}

function varius_api_get_type_ranked($type_code, $min_count=1) {
    $ret = varius_wp()->api->getTypeRanked($type_code, $min_count);

    $T = $ret->body->data;
    return $ret->body->data;
}

function varius_api_get_typegroup_type_ranked($type_code, $typegroup_code="", $min_count=1) {
    $ret = varius_wp()->api->getApiTypegroupTypeRanked($type_code, $typegroup_code, $min_count);

    $T = $ret->body->data;
    return $ret->body->data;
}

function varius_api_get_typegroup_ranked($typegroup_code, $min_count=1) {
    $ret = varius_wp()->api->getApiTypegroupRanked($typegroup_code, $min_count);

    $G = $ret->body->data;

    $G->url = varius_wp_url(['typegroup_code' => $G->code]);

    foreach ( $G->types as $T ) {
        $T->url = varius_wp_url(['typegroup_code' => $G->code, 'type_code' => $T->code]);
    }

    return $G;
}

function varius_api_get_typegroups_ranked($min_count=1) {

    $ret = varius_wp()->api->getApiTypegroupsRanked($min_count);

    $typegroups = $ret->body->data->typegroups;

    foreach ( $typegroups as $G ) {
        $G->url = varius_wp_url(['typegroup_code' => $G->code]);

        foreach ( $G->types as $T ) {
            $T->url = varius_wp_url(['typegroup_code' => $G->code, 'type_code' => $T->code]);
        }
    }

    return $typegroups;
}


function varius_api_get_listing($stock_number) {
    $ret = varius_wp()->api->getListing($stock_number);

    $listing = $ret->body->data;
    $listing->url = varius_wp_url(['stock_number' => $listing->stock_number]);

    return $listing;
}

function varius_api_search_listings($search_params, &$pagination_data) {
    $search_params['typemodule_code'] = 'api';

    $ret = varius_wp()->api->searchListings($search_params);

    $listings = $ret->body->data;

    foreach ( $listings as $L ) {
        $L->url = varius_wp_url(['stock_number' => $L->stock_number]);
    }

    if ( isset($ret->body->pagination) ) {
        $pagination_data = $ret->body->pagination;
    } else {
        $pagination_data = null;
    }

    return $listings;
}
