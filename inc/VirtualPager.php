<?php namespace Helpers;

use DateTime;
use DateTimeZone;
use WP_Term;

// Adapted from https://www.exratione.com/2016/04/wordpress-4-create-virtual-pages-in-code-without-page-database-entries/
// And https://gist.github.com/brianoz/9105004

define('VARIUS_PAGE_NAME_MAIN', 'page-varius-main');
define('VARIUS_PAGE_NAME_ERROR', 'page-varius-error');
define('VARIUS_PAGE_NAME_TYPE_ARCHIVE', 'archive-type');
define('VARIUS_PAGE_NAME_MAKE_ARCHIVE', 'archive-make');
define('VARIUS_PAGE_NAME_LISTING', 'single-listing');

class VirtualPager {

    public function __construct() {

        add_filter('query_vars', array(&$this, 'add_query_vars'));
        add_action('init', array(&$this, 'add_rewrite_rules'));
        add_filter('term_link', array(&$this, 'term_link'), 10, 3);

        // Virtual pages are checked in the 'parse_request' filter.
        // This action starts everything off if we are a virtual page
        add_action('parse_request', array(&$this, 'parse_request'));
    }

    function parse_request(&$wp) {

        if ( empty($wp->query_vars['varius-wp-page']) ) { return; }

        if ( varius_wp()->getSetting('dev_mode') && ! (is_user_logged_in()) ) { return; }

        add_action('template_include', array(&$this, 'pick_template'));
        add_filter('the_posts', array(&$this, 'the_posts'), 10, 2);

        add_filter('wp_title', array(&$this, 'wp_title'), 10, 3);
    }

    function wp_title($title, $sep, $seplocation) {
        if ( $seplocation == 'right' ) {
            return $title . get_bloginfo('name');
        } else {
            return get_bloginfo('name') . $title;
        }
    }

    function the_posts($posts, $query) {

        if ( ! array_key_exists('varius-wp-page', $query->query_vars)
                || $query->query_vars['post_type'] != 'post' ) { return $posts; }

        $pagename = $query->query_vars['varius-wp-page'];
        switch ( $pagename ) {
            case VARIUS_PAGE_NAME_MAIN :
                return $this->makeMainPage();
            case VARIUS_PAGE_NAME_TYPE_ARCHIVE :
                if ( array_key_exists('varius-wp-type', $query->query_vars) ) {
                    return $this->makeListingPostsForType($query->query_vars['varius-wp-typegroup'], $query->query_vars['varius-wp-type']);
                } else {
                    return $this->makeListingPostsForType($query->query_vars['varius-wp-typegroup']);
                }
            case VARIUS_PAGE_NAME_MAKE_ARCHIVE :
                return $this->makeListingPostsForMake($query->query_vars['varius-wp-make']);
            case VARIUS_PAGE_NAME_LISTING:
                $wp_query->is_single = true;
                return $this->makeListingPost($query->query_vars['varius-wp-stocknum']);
            default:
                break;
        }


    }

    function term_link($termlink, $term, $taxonomy) {
// var_dump($termlink);
// var_dump($term);
// var_dump($taxonomy);
// die();
        if ( $taxonomy == 'listingtype' ) {
            // Use our hijacked taxonomy term to produce the correct url for it
            $type_data = explode(":", $term->slug);
            if ( count($type_data) == 1 ) {
                return varius_wp_url(['typegroup_code' => $type_data[0]]);
            } elseif ( count($type_data) > 1 ) {
                return varius_wp_url(['typegroup_code' => $type_data[0], 'type_code' => $type_data[1]]);
            }
        }
return $termlink;
    }

    function makeErrorPage($error) {
        $status = $error->getResponse()->getStatusCode();

        status_header($status);

        $post = $this->make_dummy_post([
                'post_title' => "Error " . $status,
                'post_content' => json_decode($error->getResponse()->getBody())->message,
                'post_name' => VARIUS_PAGE_NAME_ERROR,
                'guid' => home_url() . varius_wp()->getSetting('url_base') . VARIUS_PAGE_NAME_ERROR
            ]);

        self::setWpQuery([
                'post' => $post,
                'is_404' => true
            ]);

        return [$post];
    }

    function makeMainPage() {

        $post = $this->make_dummy_post([
                'post_title' => varius_wp()->getSetting('main_page_title'),
                'post_name' => VARIUS_PAGE_NAME_MAIN,
                'guid' => home_url() . varius_wp()->getSetting('url_base') . VARIUS_PAGE_NAME_MAIN
            ]);

        self::setWpQuery([
                'post' => $post,
                'posts' => [$post],
                'queried_object' => $post,
                'queried_object_id' => $post->ID
            ]);

        return [$post];
    }

    function makeListingPost($stock_number) {

        try {
            $listing = varius_api_get_listing($stock_number);
        } catch ( \GuzzleHttp\Exception\RequestException $e ) {
            $status = $e->getResponse()->getStatusCode();
            $body = json_decode($e->getResponse()->getBody());
            if ( $status == 404 && $body->error == 'unknown_stock_number' ) {
                wp_redirect(varius_wp_url() . "?unknown_listing=" . rawurlencode($stock_number), 301);
                exit();
            } else {
                $this->makeErrorPage($e);
            }
        }

        if ( $listing ) {

            $post = $this->make_dummy_post([
                    'post_title' => $listing->title,
                    //'post_type' => 'varius_listing',
                    'post_name' => $listing->stock_number,
                    'post_date' => $listing->meta->created,
                    'modified' => $listing->meta->updated,
                    'guid' => varius_wp_url(['stock_number' => $listing->stock_number ])
                ]);
            $post->varius_listing = $listing;

            self::setWpQuery([
                    'is_page' => false,
                    'post' => $post,
                    'posts' => [$post],
                    'queried_object' => $post,
                    'queried_object_id' => $post->ID
                ]);

            return [$post];
        }

    }

    function makeListingPostsForMake($make_code) {

        try {
            $make = varius_api_get_make_ranked($make_code);
        } catch ( \GuzzleHttp\Exception\RequestException $e ) {
            return $this->makeErrorPage($e);
        }

        $taxonomy_term = $this->make_dummy_taxonomy_term([
                'taxonomy_name' => 'listingmake',
                'taxonomy_label' => $make->name,
                'term_name' => $make->name . ' - ' . varius_wp()->getSetting('main_page_title'),
                'term_slug' => $make_code
            ]);

        // Attach the typegroup and type objects to the $taxonomy term so they're accessible from the templates using get_queried_object
        $taxonomy_term->varius_make = $make;

        $search_params['make_code'] = $make_code;
        $search_params['limit'] = varius_wp()->getSetting('listings_per_page', 10);
        $search_params['page'] = get_query_var('paged', 1);
        $search_params['order'] = varius_get_sort_order();
        $listings = varius_api_search_listings($search_params, $pagination);

        $posts = [];
        foreach ( $listings as $L ) {
            $posts[] = $this->make_dummy_post_from_listing($L);
        }

        self::setWpQueryForArchive($posts, $taxonomy_term, $pagination);

        return $posts;

    }

    function makeListingPostsForType($typegroup_code, $type_code = '') {

        $typegroup = varius_api_get_typegroup_ranked($typegroup_code);
        if ( $type_code ) {
            $type = varius_api_get_typegroup_type_ranked($type_code, $typegroup_code);
        }

        if ( $type_code ) {
            $search_params = ['type_code' => $type_code];
        } else {
            $search_params = ['typegroup_code' => $typegroup_code];
        }

        // This will get used as the page title; use different titles for types and typegroups
        if ( $type && count($type) && $type[0]->code ) {
            $term_name = $type[0]->name . ' - ' . $typegroup->name . ' - ' . varius_wp()->getSetting('main_page_title');
        } else {
            $term_name = $typegroup->name . ' - ' . varius_wp()->getSetting('main_page_title');
        }

        $taxonomy_term = $this->make_dummy_taxonomy_term([
                'taxonomy_label' => $typegroup->name,
                'taxonomy_name' => 'listingtype',
                'term_name' => $term_name,
                'term_slug' => $typegroup_code . ( $type_code ? ":" . $type_code : '' ),
            ]);

        // Attach the typegroup and type objects to the $taxonomy term so they're accessible from the templates using get_queried_object
        $taxonomy_term->varius_typegroup = $typegroup;
        if ( $type_code ) {
            $taxonomy_term->varius_type = $type;
        }

        $search_params['limit'] = varius_wp()->getSetting('listings_per_page', 10);
        $search_params['order'] = varius_get_sort_order();
        $search_params['page'] = get_query_var('paged', 1);
        $listings = varius_api_search_listings($search_params, $pagination);

        $posts = [];
        foreach ( $listings as $L ) {
            $posts[] = $this->make_dummy_post_from_listing($L);
        }

        self::setWpQueryForArchive($posts, $taxonomy_term, $pagination);

        return $posts;
    }

    static function setWpQueryForArchive($posts, $taxonomy_term, $pagination) {
        self::setWpQuery([
                'is_page' => false,
                'is_singular' => false,
                'is_archive' => true,
                'is_tax' => true,
                'post' => isset($posts[0]) ? $posts[0] : null,
                'posts' => $posts,
                'queried_object' => $taxonomy_term,
                'queried_object_id' => $taxonomy_term->term_id,
                'post_count' => $pagination->page_items,
                'found_posts' => $pagination->total_items,
                'max_num_pages' => $pagination->pages
            ]);
    }

    static function setWpQuery($data) {
        global $wp, $wp_query;

        $wp_query->is_page = isset($data['is_page']) ? $data['is_page'] : TRUE;
        $wp_query->is_singular = isset($data['is_singular']) ? $data['is_singular'] : TRUE;
        $wp_query->is_home = FALSE;
        $wp_query->is_archive = isset($data['is_archive']) ? $data['is_archive'] : FALSE;
        $wp_query->is_tax = isset($data['is_tax']) ? $data['is_tax'] : FALSE;
        $wp_query->is_category = isset($data['is_category']) ? $data['is_category'] : FALSE;
        unset($wp_query->query['error']);
        $wp->query = array();
        $wp_query->query_vars['error'] = '';
        $wp_query->is_404 = isset($data['is_404']) ? $data['is_404'] : FALSE;
        $wp_query->current_post = -1; //$p->ID;
        $wp_query->found_posts = isset($data['found_posts']) ? $data['found_posts'] : 1;
        $wp_query->post_count = isset($data['post_count']) ? $data['post_count'] : 1;
        $wp_query->max_num_pages = isset($data['max_num_pages']) ? $data['max_num_pages'] : 1;
        $wp_query->comment_count = 0;
        // -1 for current_comment displays comment if not logged in!
        $wp_query->current_comment = null;
        $wp_query->post = $data['post'];
        $wp_query->posts = isset($data['posts']) ? $data['posts'] : array($data['post']);
        $wp_query->queried_object = isset($data['queried_object']) ? $data['queried_object'] : $data['post'];
        $wp_query->queried_object_id = isset($data['queried_object_id']) ? $data['queried_object_id'] : $data['post']->ID;
    }

    function make_dummy_taxonomy_term($data) {
        global $wp_taxonomies;

        // Trick WP into thinking we have a proper taxonomy
        $wp_taxonomies[$data['taxonomy_name']] = (object)[
            'labels' => (object)[ 'name' => $data['taxonomy_label'] ],
            'query_var' => $data['taxonomy_name'],
            'object_type' => 'post',
            'hierarchical' => false];

        // Make a dummy tax term so we get a proper page title from wp_title()
        $taxonomy_term = new \WP_Term((object)[
                'term_id' => -1,
                'name' => $data['term_name'],
                'slug' => $data['term_slug'],
                'taxonomy' => $data['taxonomy_name']
            ]);

        return $taxonomy_term;
    }

    function make_dummy_post_from_listing($listing) {
        $post_name = strtolower($listing->title);
        $post_name = preg_replace("/\s+/", "_", $post_name);
        $post_name = preg_replace("/[^A-Za-z0-9_]/", "", $post_name);

        $post = $this->make_dummy_post([
                'post_title' => $listing->title,
                'post_name' => $post_name,
                'guid' => home_url() . varius_wp()->getSetting('url_base') . $post_name
            ]);

        $post->varius_listing = $listing;

        return $post;
    }

    function make_dummy_post($data) {
        // have to create a dummy post as otherwise many templates
        // don't call the_content filter
        global $wp, $wp_query;

        if ( isset($data['post_date']) ) {
            $date = (new DateTime($data['post_date'], new DateTimeZone(date_default_timezone_get())));
            $date_gmt = clone $date;
            $date_gmt->setTimezone(new DateTimeZone('GMT'));
            $date = $date->format("Y-m-d H:i:s");
            $date_gmt = $date_gmt->format("Y-m-d H:i:s");
        } else {
            $date = current_time('mysql');
            $date_gmt = current_time('mysql', $gmt = 1);
        }

        if ( isset($data['modified']) ) {
            $date_modified = new DateTime($data['modified'], new DateTimeZone(date_default_timezone_get()));
            $date_modified_gmt = clone $date_modified;
            $date_modified_gmt->setTimezone(new DateTimeZone('GMT'));
            $date_modified = $date_modified->format("Y-m-d H:i:s");
            $date_modified_gmt = $date_modified_gmt->format("Y-m-d H:i:s");
        } else {
            $date_modified = $date;
            $date_modified_gmt = $date_gmt;
        }

        //create a fake post intance
        $p = [
            'ID' => -1,
            'post_author' => 1,
            'post_date' => $date,
            'post_date_gmt' =>  $date_gmt,
            'post_content' => isset($data['post_content']) ? $data['post_content'] : '',
            'post_title' => '',
            'post_excerpt' => '',
            'post_status' => 'publish',
            'ping_status' => 'closed',
            'post_password' => '',
            'post_name' => $data['post_name'],
            'to_ping' => '',
            'pinged' => '',
            'modified' => "". $date_modified,
            'modified_gmt' => "". $date_modified_gmt,
            'post_content_filtered' => '',
            'post_parent' => 0,
            'guid' => $data['guid'],
            'menu_order' => 0,
            'post_type' => 'post',
            'post_mime_type' => '',
            'comment_status' => 'closed',
            'comment_count' => 0,
            'filter' => 'raw',
            'ancestors' => array()
        ];
        // fill $p with everything a page in the database would have

        $p = array_merge($p, $data);

        return (object)$p;
    }

    function add_query_vars($vars) {
        $vars[] = 'varius-wp-page';
        $vars[] = 'varius-wp-typegroup';
        $vars[] = 'varius-wp-type';
        $vars[] = 'varius-wp-make';
        $vars[] = 'varius-wp-stocknum';
        $vars[] = 'varius-wp-order';
        return $vars;
    }
    // add_filter('query_vars', 'varius_wp_add_query_vars');

    function add_rewrite_rules() {
        $url_base = trim(varius_wp()->getSetting('url_base'), "/");


        add_rewrite_tag('%varius-wp-page%', '([^&]+)');
        add_rewrite_tag('%varius-wp-typegroup%', '([^&]+)');
        add_rewrite_tag('%varius-wp-type%', '([^&]+)');
        add_rewrite_tag('%varius-wp-make%', '([^&]+)');
        add_rewrite_tag('%varius-wp-stocknum%', '([^&]+)');
        add_rewrite_rule($url_base . '/?$', 'index.php?varius-wp-page='. VARIUS_PAGE_NAME_MAIN, 'top');
        add_rewrite_rule($url_base . '/make/([^/]*)(/page/([0-9]+)/?)?$', 'index.php?varius-wp-page=' . VARIUS_PAGE_NAME_MAKE_ARCHIVE . '&varius-wp-make=$matches[1]&paged=$matches[3]', 'top');
        add_rewrite_rule($url_base . '/type/([^/]*)(/page/([0-9]+)/?)?$', 'index.php?varius-wp-page=' . VARIUS_PAGE_NAME_TYPE_ARCHIVE . '&varius-wp-typegroup=$matches[1]&paged=$matches[3]', 'top');
        add_rewrite_rule($url_base . '/type/([^/]*)/([^/]*)(/page/([0-9]+)/?)?$', 'index.php?varius-wp-page=' . VARIUS_PAGE_NAME_TYPE_ARCHIVE . '&varius-wp-typegroup=$matches[1]&varius-wp-type=$matches[2]&paged=$matches[4]', 'top');
        add_rewrite_rule($url_base . '/listing/([^/]*)/?$', 'index.php?varius-wp-page=' . VARIUS_PAGE_NAME_LISTING . '&varius-wp-stocknum=$matches[1]', 'top');
    }


    function pick_template($template) {
        global $wp_query;
        $new_template = '';

        if ( ! array_key_exists('varius-wp-page', $wp_query->query_vars) ) { return $template; }

        if ( isset($wp_query->post) && $wp_query->post->post_name == VARIUS_PAGE_NAME_ERROR ) {
            $pagename = "error";
        } else {
            $pagename = $wp_query->query_vars['varius-wp-page'];
        }

        $template_name = "$pagename.php";

        // Look for a template in the theme...
        $template_path = locate_template(array("varius/$template_name"));
        // ...if none found, use the plugin's default template
        if ( $template_path == '' ) {
            $template_path = plugin_dir_path(dirname(__FILE__)) . 'theme/' . $template_name;
        }

        return $template_path;

    }

}

