<?php

if ( ! current_user_can('manage_options') ) { return ; }

?><div class="wrap" id="varius-wp-api-settings">
    <h1><?php echo esc_html(get_admin_page_title()) ?></h1>
    <form action="options.php" method="post">
        <?php
        settings_fields('varius_settings');
        do_settings_sections('varius_settings');
        submit_button('Save Settings');
        ?>
    </form>
</div>