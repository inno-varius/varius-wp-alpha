<?php if ( ! defined( 'ABSPATH' ) ) { die; } // If this file is called directly, abort.

define('VARIUS_WP_DO_FLUSH_REWRITE_RULES', 'varis-wp-do-flush-rewrite-rules');

class varius_admin {

    public function __construct() {
        add_action('admin_menu', array($this, 'varius_admin_menu'));
        add_action('admin_init', array($this, 'varius_admin_init'));
        add_action( 'admin_enqueue_scripts', array($this, 'inject_assets'));
        add_action('update_option_' . VARIUS_WP_OPTION_NAME, array($this, 'on_settings_saved'), 10, 2);

        // We use a transient because flushing directly in on_settings_saved doesn't actually wait.
        // We have to wait until the next page load for the flush to work.
        // See: http://wordpress.stackexchange.com/questions/210596/flush-rewrite-rules-not-working-when-settings-updated
        if ( delete_transient(VARIUS_WP_DO_FLUSH_REWRITE_RULES) ) {
            add_action('admin_init', array($this, 'flush_rewrite_rules')); // so the flush doesn't run before it is able to
        }
    }

    function flush_rewrite_rules() {
        flush_rewrite_rules();
    }

    function on_settings_saved($old, $new) {
        if ( $old['url_base'] != $new['url_base'] ) {
            set_transient(VARIUS_WP_DO_FLUSH_REWRITE_RULES);  // mark that a flush needs to occur
        }
    }

    function varius_admin_menu() {
        add_submenu_page(
            'options-general.php',
            'Varius Settings',
            'Varius',
            varius_wp()->config('capability'), // 'manage_options',
            plugin_dir_path(__FILE__) . 'view.php'
        );
    }

    function sanitize_settings($input) {
        return $input;
    }

    function varius_admin_init() {
        register_setting('varius_settings', VARIUS_WP_OPTION_NAME, array($this, 'sanitize_settings'));

        add_settings_section(
            'varius_settings_section__site',
            'Site Settings',
            null,
            'varius_settings'
        );

        add_settings_field(
            'varius_setting__dev_mode',
            'Dev Mode',
            array($this, 'render_setting__dev_mode'),
            'varius_settings',
            'varius_settings_section__site'
        );

        add_settings_field(
            'varius_setting__url_base',
            'URL Base',
            array($this, 'render_setting__url_base'),
            'varius_settings',
            'varius_settings_section__site'
        );

        add_settings_field(
            'varius_setting__main_page_title',
            'Main Page Title',
            array($this, 'render_setting__main_page_title'),
            'varius_settings',
            'varius_settings_section__site'
        );

        add_settings_field(
            'varius_setting__listings_per_page',
            'Listings Per Page',
            array($this, 'render_setting__listings_per_page'),
            'varius_settings',
            'varius_settings_section__site'
        );

        // add_settings_field(
        //     'varius_setting__redirect_single_listing_types',
        //     'Redirect to Single Listings',
        //     array($this, 'render_setting__redirect_single_listing_types'),
        //     'varius_settings',
        //     'varius_settings_section__site'
        // );

        add_settings_section(
            'varius_settings_section__api',
            'API Settings',
            null, // callback for section
            'varius_settings'
        );


        add_settings_field(
            'varius_setting__api_test', // as of WP 4.6 this value is used only internally
            'API Auth Test',
            array($this, 'render_setting__api_test'),
            'varius_settings',
            'varius_settings_section__api',
            array(

                )
        );

        add_settings_field(
            'varius_setting__api_base_uri', // as of WP 4.6 this value is used only internally
            'API Base URI',
            array($this, 'render_setting__api_base_uri'),
            'varius_settings',
            'varius_settings_section__api',
            array(

                )
        );

        add_settings_field(
            'varius_setting__dealer_code', // as of WP 4.6 this value is used only internally
            'Dealer Code',
            array($this, 'render_setting__dealer_code'),
            'varius_settings',
            'varius_settings_section__api',
            array(

                )
        );

        add_settings_field(
            'varius_setting__api_access_token', // as of WP 4.6 this value is used only internally
            'API Access Token',
            array($this, 'render_setting__api_access_token'),
            'varius_settings',
            'varius_settings_section__api',
            array(

                )
        );

    }

    function validate_api_dealer_code($input) {
        return true;
    }

    function render_setting__dev_mode($args) {
        ?><input type="checkbox" name="<?php echo VARIUS_WP_OPTION_NAME ?>[dev_mode]" value="1" <?php echo esc_attr(varius_wp()->getSetting('dev_mode')) ? 'checked' : '' ?>>
        <small><span class="muted">If this is checked, only logged in users will be able to view the pages generated by this plugin.</span></small><?php
    }

    function render_setting__url_base($args) {
        ?><input type="text" name="<?php echo VARIUS_WP_OPTION_NAME ?>[url_base]" value="<?php echo esc_attr(varius_wp()->getSetting('url_base')) ?>"><br>
        <small><span class="muted">The url (relative to <?php echo home_url() ?>) at which the Varius plugin should operate.  Eg. '/used_equipment/'.</span></small><?php
    }

    function render_setting__main_page_title($args) {
        ?><input type="text" name="<?php echo VARIUS_WP_OPTION_NAME ?>[main_page_title]" value="<?php echo esc_attr(varius_wp()->getSetting('main_page_title')) ?>"><br>
        <small><span class="muted">The title for the main landing page at URL Base above.  Eg. 'Used Equipment'.</span></small><?php
    }

    function render_setting__listings_per_page($args) {
        ?><input type="number" step="1" min="1" name="<?php echo VARIUS_WP_OPTION_NAME ?>[listings_per_page]" value="<?php echo esc_attr(varius_wp()->getSetting('listings_per_page')) ?>" >
        <small><span class="muted">The number of listings to show on per page when multiple listings are shown.</span></small><?php
    }

    function render_setting__redirect_single_listing_types($args) {
        ?><input type="checkbox" name="<?php echo VARIUS_WP_OPTION_NAME ?>[redirect_single_listing_types]" value="1" <?php echo esc_attr(varius_wp()->getSetting('redirect_single_listing_types')) ? 'checked' : '' ?>>
        <small><span class="muted">When a user browses to a type page with only a single listing, automatically redirect to that listing's page.</span></small><?php
    }

    function render_setting__api_base_uri($args) {
        ?><input type="text" name="<?php echo VARIUS_WP_OPTION_NAME ?>[api_base_uri]" value="<?php echo esc_attr(varius_wp()->getSetting('api_base_uri')) ?>"><br>
        <small><span class="muted">The Varius api base URL (trailing slash required) Eg. 'http://portal.varius.ca/'.</span></small><?php
    }

    function render_setting__api_access_token($args) {
        ?><input type="text" class="api-access-token" name="<?php echo VARIUS_WP_OPTION_NAME ?>[api_access_token]" value="<?php echo esc_attr(varius_wp()->getSetting('api_access_token')) ?>"><?php
    }


    function render_setting__dealer_code($args) {
        ?><input type="text" name="<?php echo VARIUS_WP_OPTION_NAME ?>[dealer_code]" value="<?php echo esc_attr(varius_wp()->getSetting('dealer_code')) ?>"><?php
    }

    function render_setting__api_test($args) {
        $ok = true;
        $message = "";
        if ( ! varius_wp()->getSetting('api_base_uri') ) {
            $ok = false;
            $message = "You need an API Base Uri";
        } elseif ( ! varius_wp()->getSetting('api_access_token') ) {
            $ok = false;
            $message = "You need an API Access Token";
        } elseif ( ! varius_wp()->getSetting('dealer_code') ) {
            $ok = false;
            $message = "You need a Dealer Code";
        } else {
            try {
                $response = varius_api_test();
            } catch ( \GuzzleHttp\Exception\ConnectException $e ) {
                $ok = false;
                $message = "Could not connect to the API.  It is likely that your Base URI is incorrect, but it could also be that the Varius API server is currently inaccessible.";
            } catch ( \GuzzleHttp\Exception\RequestException $e ) {

                $ok = false;
                $message = "Unknown error";

                if ( $e->getResponse() ) {
                    $status = $e->getResponse()->getStatusCode();
                    $body = json_decode($e->getResponse()->getBody());

                    if ( $status == 404 ) {
                        if ( isset($body->error) && $body->error == 'unknown_dealer_code' ) {
                            $message = "Your Dealer Code was not recognized.";
                        } else {
                            $message = "Your Base URI seems to be incorrect.";
                        }
                    } elseif ( $status == 401 ) {
                        $message = "Your Access Token does not correspond to your Dealer Code.  One or the other is incorrect.";
                    }
                }

            }
        } ?><div class="<?php echo $ok ? 'success' : 'danger' ?>"><?php echo $ok ? 'Passed' : 'Failed:<br>' ?>
            <?php echo $message ?>
        </div>
        <small><span class="muted">Save Settings or Refresh this page to run the auth test again.</span></small><?php
    }

    function inject_assets() {
        wp_enqueue_style('varius-wp-admin-style', plugins_url( 'assets/admin.css', __FILE__ ));
    }

}

new varius_admin();